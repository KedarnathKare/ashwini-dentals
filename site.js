$('.owl-carousel.gallery-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    dots:false,
    autoplay: true,
    responsive:{
        0:{
            items:1
        }
    }
});

$('.owl-carousel.service-owl').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    items:3,
    responsiveClass: true,
    responsive:{
        768:{
            items:3
        },
        575:{
            items:2
        },
        320:{
            items:1
        }
    }
});